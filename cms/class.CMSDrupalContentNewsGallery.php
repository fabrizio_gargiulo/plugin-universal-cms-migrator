<?php

class CMSDrupalContentNewsGallery extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_photo as $field) {
            $this->addExtraFieldFile('field_photo', $field);
        }

        return $this;
    }
}