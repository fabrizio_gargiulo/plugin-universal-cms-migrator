<?php

class CMSDrupalContentArticleFlipBook extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_url_article as $field) {
            $this->addExtraFieldText('field_url_article', $field);
        }

        return $this;
    }
}

/**
[field_url_article] => Array
        (
            [0] => Array
                (
                    [value] => http://accademiaegitto.git/sites/default/files/article/article1/article1.html
                )

        )
 */