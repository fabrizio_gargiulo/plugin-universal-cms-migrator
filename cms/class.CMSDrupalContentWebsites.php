<?php

class CMSDrupalContentWebsites extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_websitelink as $field) {
            $this->addExtraFieldText('field_websitelink', $field);
        }

        foreach ($node->field_website_webshot as $field) {
            $this->addExtraFieldFile('field_website_webshot', $field);
        }

        return $this;
    }
}