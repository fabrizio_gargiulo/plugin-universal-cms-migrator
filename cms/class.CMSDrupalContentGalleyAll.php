<?php

class CMSDrupalContentGalleyAll extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_gallery_photo as $field) {
            $this->addExtraFieldFile('field_gallery_photo', $field);
        }

        return $this;
    }
}