<?php

class CMSDrupalContentVideoYoutube extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        // @dafare: in questo caso oltre alla url del video c'� anche la url della copertina, che faccio la importo?
        // @dafare: e se ci sono pi� coppie di video/copertina faccio una sola variabile e la serializzo?
        foreach ($node->field_video_url as $key => $field) {
            if (empty($field)) continue;

            $this->addExtraField('field_video_url', $field['embed']);
        }

        return $this;
    }
}

/**
[field_video_url] => Array
        (
            [0] => Array
                (
                    [embed] => http://www.youtube.com/watch?v=Dl1k2_OFvww
                    [value] => Dl1k2_OFvww
                    [provider] => youtube
                    [data] => Array
                        (
                            [emvideo_data_version] => 5
                            [emvideo_youtube_version] => 5
                            [duration] => 1173
                            [playlist] => 0
                            [thumbnail] => Array
                                (
                                    [url] => http://img.youtube.com/vi/Dl1k2_OFvww/0.jpg
                                )

                            [flash] => Array
                                (
                                    [url] => http://youtube.com/v/Dl1k2_OFvww
                                    [size] => 0
                                    [mime] => application/x-shockwave-flash
                                )

                            [emthumb] => Array
                                (
                                    [uid] => 3
                                    [filename] => emvideo-youtube-Dl1k2_OFvww.jpg
                                    [filepath] => sites/default/files/emvideo-youtube-Dl1k2_OFvww.jpg
                                    [filemime] => image/jpeg
                                    [source] => emthumb_fetch_remote_thumbnail
                                    [destination] => sites/default/files/emvideo-youtube-Dl1k2_OFvww.jpg
                                    [filesize] => 10364
                                    [status] => 1
                                    [timestamp] => 1359994142
                                    [fid] => 332
                                )

                        )

                    [status] => 1
                    [version] => 5
                    [title] =>
                    [description] =>
                    [duration] => 1173
                )

        )
 */