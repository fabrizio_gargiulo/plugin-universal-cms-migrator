<?php

define ("CONTENT_TYPE_PRODUCT", 'product');

class CMSJoomlaExporter extends AbstractCMSExporter
{
    protected function _get_all($table)
    {
        $query = "SELECT * FROM #__".$table;
        $db = JFactory::getDBO();
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    protected function _get_contents()
    {
        $query = "SELECT * FROM #__content WHERE `state` = 1";
        $db = JFactory::getDBO();
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    protected function _get_cat_by_id($id)
    {
        $query = "SELECT * FROM #__categories WHERE id = ".$id;
        $db = JFactory::getDBO();
        $db->setQuery($query);

        $list = $db->loadObjectList();

        if (empty($list)) {
            return array();
        }

        return $list[0];
    }

    public function getUsers()
    {
        return array();
    }

    public function getTaxonomies()
    {
        $array = array();
        $list = array();

        $taxonomy = new Taxonomy(1, "categories", "Categories");
        $categories = $this->_get_all("categories");

        foreach ($categories as $cat) {
            $term = new TaxonomyTerm($taxonomy, $cat->id, $cat->title, $cat->alias);

            $term->setParent($cat->parent_id);
            $array[$cat->parent_id][] = $cat->id.'# '.$cat->title;
            $taxonomy->addTerm($term);
        }

        $list[] = $taxonomy;

        return $list;
    }

    public function getContentTypes()
    {
        $content_types = array();

        $content_types[] = new ContentType("article", "Article");
        $content_types[] = new ContentType("page", "Page");
        $content_types[] = new ContentType(CONTENT_TYPE_PRODUCT, "Prodotti");

        return $content_types;
    }

    public function getMenuProducts()
    {
        $query = "SELECT * FROM #__menu WHERE menutype = 'menu-prodotti' order by level ASC, rgt asc";
        $db = JFactory::getDBO();
        $db->setQuery($query);

        return $db->loadObjectList();
    }

    // menu item id => article id
    public function getMappingFromMenuItems()
    {
        $menu_item_maps = array(1 => 0);
        $article_maps = array();

        $query = "SELECT * FROM #__menu WHERE menutype = 'menu-prodotti' order by level ASC, rgt asc";
        $db = JFactory::getDBO();
        $db->setQuery($query);

        foreach ($this->getMenuProducts() as $row) {
            $parent_id = 0;

            $article_id = str_replace("index.php?option=com_content&view=article&id=", "", $row->link);

            if (empty($article_id) || !is_numeric($article_id)) {
                die("Row link: ".$row->link);
                continue;
            }

            $menu_item_maps[$row->id] = $article_id;

            if (!empty($menu_item_maps[$row->parent_id])) {
                $parent_id = $menu_item_maps[$row->parent_id];
            }

            $url = new JURI('index.php/'.$row->path);

            $article_maps[$article_id] = array(
                'parent_id' => $parent_id,
            	'menuid' => $row->id,
                'url' => $url->__toString()
            );
        }

        return $article_maps;
    }

    public function getContents()
    {
        /**
         * IMPORTANTE
         * Gli articoli in PB non hanno parent, la gerarchia viene realizzata tramite il menu prodotti.
         * Ogni menu item punta ad uno specifico articolo.
         *
         * Quello che bisogna fare e' traversare la struttura del menu, ricavare l'id articolo di ogni
         * menu item, e associa l'id del menu item all'id del relativo articolo, servira' per ricavare
         * il parent article id dal parent menu item id
         * @var unknown_type
         */
        $contents = array();
        $mappings = $this->getMappingFromMenuItems();

        $rows = $this->_get_contents();

        $taxonomy = new Taxonomy(1, 'categories', 'Categorie');

        foreach($rows as $row){
            $category = $this->_get_cat_by_id($row->catid);

            $content = CMSJoomlaContent::getByArticleID($row->id);

            if (!empty($category)) {
                $term = new TaxonomyTerm($taxonomy, $category->id, $category->title, $category->alias);
                $term->setParent($category->parent_id);
                $content->addTerm($term);
            }

            if (!empty($mappings[$row->id])) {
                $content->setParentID($mappings[$row->id]['parent_id']);
                $content->setUrl($mappings[$row->id]['url']);
                $content->setContentType(CONTENT_TYPE_PRODUCT);
                $content->setMenuID($mappings[$row->id]['menuid']);

                $catalogue = $content->getCatalogue();

                if (!empty($catalogue)) {
                    $content->addExtraField('catalogue', $catalogue);
                }
            }

            $contents[] = $content;
        }

        return $contents;
    }
}