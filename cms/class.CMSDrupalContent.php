<?php

class CMSDrupalContent extends Content
{
    static public function getByNode($node)
    {
        switch ($node->type) {
            case 'mission':
                $content = new CMSDrupalContentMission($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'page':
                $content = new CMSDrupalContentPage($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'event_date':
                $content = new CMSDrupalContentEventData($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'image':
                $content = new CMSDrupalContentImage($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'news_gallery':
                $content = new CMSDrupalContentNewsGallery($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'galley_all':
                $content = new CMSDrupalContentGalleyAll($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'newsletter':
                $content = new CMSDrupalContentNewsletter($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'video_youtube':
                $content = new CMSDrupalContentVideoYoutube($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'story':
                $content = new CMSDrupalContentStory($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'article_flip_book':
                $content = new CMSDrupalContentArticleFlipBook($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            case 'websites':
                $content = new CMSDrupalContentWebsites($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                $content->initByNode($node);
                break;

            default:
                // Content type default/generico
                $content = new Content($node->nid, $node->title, url('node/'. $node->nid), $node->body, $node->type);
                break;
        }

        return $content;
    }

    public function addExtraFieldFile($field_name, $field)
    {
        if (empty($field)) return $this;

        $image_url = file_create_url($field['filepath']);
        return $this->addExtraField($field_name, $image_url);
    }

    public function addExtraFieldText($field_name, $field)
    {
        if (empty($field)) continue;

        return $this->addExtraField($field_name, $field['value']);;
    }

    public function initByNode($node)
    {
        $this->setLanguage($node->language);

        if (!empty($node->teaser)) {
            $this->setAbstract($node->teaser);
        }

        if (!empty($node->taxonomy)) {
            foreach ($node->taxonomy as $t) {
                $term = CMSDrupalTaxonomy::createTerm($t->vid, $t->tid, $t->name, $t->description);
                $this->addTerm($term);
            }
        }

        return $this;
    }
}