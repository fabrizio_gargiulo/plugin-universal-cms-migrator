<?php

class CMSDrupalContentNewsletter extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        if (isset($node->field_news_file)) {
            foreach ($node->field_news_file as $field) {
                $this->addExtraFieldFile('field_news_file', $field);
            }
        }

        if (isset($node->field_screenshot)) {
            foreach ($node->field_screenshot as $field) {
                $this->addExtraFieldFile('field_screenshot', $field);
            }
        }

        if (isset($node->field_url)) {
            foreach ($node->field_url as $field) {
                $this->addExtraFieldText('field_url', $field);
            }
        }

        return $this;
    }
}