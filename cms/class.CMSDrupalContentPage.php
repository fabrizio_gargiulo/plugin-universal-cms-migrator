<?php

class CMSDrupalContentPage extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_photo_sub as $key => $field) {
            $this->addExtraFieldFile('field_photo_sub', $field);
        }

        return $this;
    }
}