<?php

class CMSDrupalContentEventData extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_event_date as $field) {
            if (empty($field)) continue;

            $this->addExtraField('field_event_date_value', $field['value']);
            $this->addExtraField('field_event_date_value2', $field['value2']);
            $this->addExtraField('field_event_date_timezone', $field['timezone']);
            $this->addExtraField('field_event_date_timezone_db', $field['timezone_db']);
            $this->addExtraField('field_event_date_type', $field['date']);
        }

        foreach ($node->field_calendar_photo as $field) {
            $this->addExtraFieldFile('field_calendar_photo', $field);
        }

        return $this;
    }
}