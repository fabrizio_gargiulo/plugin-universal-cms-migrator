<?php

class CMSDrupalTaxonomy extends Taxonomy
{
    public static $instances = array();

    // Optional
    protected $content_types, $relations, $hierarchy, $multiple, $required;

    public function __construct($id, $name, $description)
    {
        parent::__construct($id, $name, $description);

        $this->content_types = array();
        $this->relations = 0;
        $this->hierarchy = 0;
        $this->multiple = 0;
        $this->required = 0;

        foreach (taxonomy_get_tree($id) as $term) {
            $taxonomy_term = new CMSDrupalTaxonomyTerm($this, $term->tid, $term->name, $term->description);

            $taxonomy_term->setWeight($term->weight)
                ->setDepth($term->depth)
                ->setParent($term->parents[0]);

            $this->terms[] = $taxonomy_term;
        }
    }

    static public function get($id, $name, $description)
    {
        if (empty(self::$instances[$id])) {
            self::$instances[$id] = new CMSDrupalTaxonomy($id, $name, $description);
        }

        return self::$instances[$id];
    }

    static public function createTerm($vid, $id, $name, $description)
    {
        $taxonomy = self::$instances[$vid];

        return $taxonomy->addTerm($id, $name, $description);
    }

    public function addContentType($content_type)
    {
        $this->content_types[$content_type] = $content_type;
        return $this;
    }

    public function addContentTypes($content_types)
    {
        foreach ($content_types as $content_type) {
            $this->addContentType($content_type);
        }

        return $this;
    }

    public function hasRelations($has_relations)
    {
        $this->relations = $has_relations;
        return $this;
    }

    public function hasHierarchy($has_hierarchy)
    {
        $this->hierarchy = $has_hierarchy;
        return $this;
    }

    public function hasMultiple($has_multiple)
    {
        $this->multiple = $has_multiple;
        return $this;
    }

    public function isRequired($is_required)
    {
        $this->required = $is_required;
        return $this;
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), array(
            'relations' => $this->relations,
            'hierarchy' => $this->hierarchy,
            'multiple' => $this->multiple,
            'required' => $this->required,
            'content_types' => $this->content_types,
        ));
    }

    public function export(DOMDocument $doc)
    {
        $element = parent::export($doc);

        $element->setAttribute('relations', $this->relations);
        $element->setAttribute('hierarchy', $this->hierarchy);
        $element->setAttribute('multiple', $this->multiple);
        $element->setAttribute('required', $this->required);

        // Content types
        $content_types = $doc->createElement('associated_content_types');

        foreach ($this->content_types as $content_type) {
            $content_type_element = $doc->createElement('associated_content_type');
            $content_type_element->setAttribute('name', $content_type);
            $content_types->appendChild($content_type_element);
        }

        $element->appendChild($content_types);

        return $element;
    }

}