<?php

class CMSDrupalExporter extends AbstractCMSExporter
{
    public function getUsers()
    {
        $users = array();

        $query = "SELECT * FROM users";

        $result = db_query($query);

        while ($user = db_fetch_array($result)) {
            if (empty($user['uid'])) {
                continue;
            }

            $users[] = new User($user['uid'], $user['name'], $user['pass'], $user['mail']);
        }

        return $users;
    }

    public function getTaxonomies()
    {
        $list = array();

        $vocabularies = taxonomy_get_vocabularies();

        foreach ($vocabularies as $v) {
            $taxonomy = CMSDrupalTaxonomy::get($v->vid, $v->name, $v->description);

            $taxonomy->hasRelations($v->relations);
            $taxonomy->hasHierarchy($v->hierarchy);
            $taxonomy->hasMultiple($v->multiple);
            $taxonomy->isRequired($v->required);

            $taxonomy->addContentTypes($v->nodes);

            $list[] = $taxonomy;
        }

        return $list;
    }

    public function getContentTypes()
    {
        $content_types = array();

        $query = sprintf("SELECT * FROM %s", 'node_type');

        $result = db_query($query);

        while ($ct = db_fetch_array($result)) {
            $content_types[] = new ContentType($ct['type'], $ct['name']);
        }

        return $content_types;
    }

    public function getContents()
    {
        $contents = array();

        // escludi contenuti non pubblicati
        $query = sprintf("SELECT * FROM %s WHERE `status` = 1", 'node');

        $result = db_query($query);

        while ($row = db_fetch_array($result)) {
            $node = node_load($row['nid']);
            $content = CMSDrupalContent::getByNode($node);

            $content->setDate($row['created']);

            if (empty($content)) {
                continue;
            }

            // Se e' abbinato a delle categorie? o tassonomie? o altre cose?
            $contents[] = $content;
        }

        return $contents;
    }
}