<?php


class CMSJoomlaContent extends Content
{
    protected $event_gallery, $menuid;

    public function __construct($id, $title, $url, $body, $content_type)
    {
        parent::__construct($id, $title, $url, $body, $content_type);

        $this->menuid = 0;
    }

    public function setMenuID($menuid)
    {
        $this->menuid = $menuid;
        return $this;
    }

    public function getCatalogue()
    {
        $query = "SELECT * FROM #__modules m JOIN #__modules_menu mm ON (m.id = mm.moduleid) WHERE m.id IN (168, 195, 196) AND mm.`menuid` = ".$this->menuid;
        $db = JFactory::getDBO();
        $db->setQuery($query);

        //$prepared_query = str_replace('#__', 'rh4oz_', $db->getQuery());

        $modules = $db->loadObjectList();

        if (empty($modules)) {
            return '';
        }

        // <p><iframe src="http://e.issuu.com/embed.html#11204299/7741724" frameborder="0" width="325" height="230"></iframe></p>

        if(preg_match("/<p><iframe src=\"(.+?)\"/", $modules[0]->content, $regs)) {
            return $regs[1];
        }

        return '';
    }

    public function getEventGallery()
    {
        $regs = array();

        if(preg_match("/{eventgallery event=\"(.+)\"/", $this->body, $regs)) {
            return $regs[1];
        }

        return '';
    }

    public function getEventGalleryImages()
    {
        $list = array();

        /**
         [5] => stdClass Object
            (
                [id] => 290
                [folder] => Carousel_5m
                [file] => Carousel_5m_6.jpg
                [width] => 700
                [height] => 525
                [caption] =>
                [title] =>
                [exif] => {"fstop":"2.8","focallength":"5","model":"E3700","iso":50}
                [ordering] =>
                [ismainimage] => 0
                [ismainimageonly] => 0
                [hits] => 0
                [published] => 1
                [allowcomments] => 1
                [userid] => 938
                [modified] => 2014-06-03 18:24:54
                [created] => 2014-06-03 18:24:54
            )
         */
        $folder = $this->getEventGallery();

        if (empty($folder)) {
            return array();
        }

        $query = "SELECT id, folder, file, width, height FROM #__eventgallery_file WHERE `published` = 1 AND `folder` LIKE '{$folder}'";
        $db = JFactory::getDBO();
        $db->setQuery($query);

        foreach ($db->loadObjectList() as $row) {
             $record = (array) $row;
             $record['path'] = sprintf("/images/eventgallery/%s/%s", $record['folder'], $record['file']);
             $list[] = $record;
        }

        return $list;
    }

    static public function getByArticleID($article_id)
    {
        $query = "SELECT * FROM #__content WHERE `id` = ".$article_id;
        $db = JFactory::getDBO();
        $db->setQuery($query);

        $articles = $db->loadObjectList();

        if (empty($articles)) {
            return null;
        }

        $article = $articles[0];

        $url = new JURI('index.php?option=com_content&view=article&id='.$article->id);

        //$url = new JURI('index.php/'.$row->path);

        $content = new CMSJoomlaContent($article->id, $article->title, $url, $article->introtext, 'article');

        $content->setDate($article->created);

        return $content;
    }

    // Export
    public function export(DOMDocument $doc)
    {
        $element = parent::export($doc);

        $attachments = $doc->createElement("attachments");

        foreach ($this->getEventGalleryImages() as $image) {
            $attachment = $doc->createElement('attachments');

            $attachment->setAttribute('id', $image['id']);
            $attachment->setAttribute('folder', $image['folder']);
            $attachment->setAttribute('file', $image['file']);
            $attachment->setAttribute('path', $image['path']);
            $attachment->setAttribute('width', $image['width']);
            $attachment->setAttribute('height', $image['height']);

            $attachments->appendChild($attachment);
        }

        $element->appendChild($attachments);
        return $element;
    }
}