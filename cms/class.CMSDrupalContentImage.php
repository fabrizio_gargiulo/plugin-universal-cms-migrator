<?php

class CMSDrupalContentImage extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->images as $key => $value) {
            $this->addExtraField($key, $value);
        }

        return $this;
    }
}