<?php

class CMSDrupalContentMission extends CMSDrupalContent
{
    public function initByNode($node)
    {
        parent::initByNode($node);

        foreach ($node->field_photo_mission as $field) {
            $this->addExtraFieldFile('field_photo_mission', $field);
        }

        return $this;
    }
}