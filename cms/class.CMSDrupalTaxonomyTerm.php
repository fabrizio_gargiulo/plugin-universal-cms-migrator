<?php

class CMSDrupalTaxonomyTerm extends TaxonomyTerm
{
    // Optional
    protected $weight, $depth;

    public function __construct(CMSDrupalTaxonomy $taxonomy, $id, $name, $machine_name)
    {
        parent::__construct($taxonomy, $id, $name, $machine_name);

        $this->weight = 0;
        $this->depth = 0;
        $this->parent = 0;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
        return $this;
    }

    public function toArray()
    {
        return array_merge(parent::toArray(), array(
            'weight' => $this->weight,
            'depth' => $this->depth
        ));
    }

    public function export(DOMDocument $doc)
    {
        $element = parent::export($doc);

        $element->setAttribute('weight', $this->weight);
        $element->setAttribute('depth', $this->depth);

        return $element;
    }

}