<?php

class Content implements IExportable
{
    protected $id, $parent_id, $date, $title, $abstract, $body, $url, $fields, $content_type, $language, $terms;

    public function __construct($id, $title, $url, $body, $content_type)
    {
        $this->id = $id;
        $this->parent_id = 0;
        $this->date = "";
        $this->title = $title;
        $this->url = $url;
        $this->abstract = '';
        $this->body = html_entity_decode($body, ENT_QUOTES, "UTF-8");
        $this->content_type = $content_type;
        $this->language = 'default';

        $this->fields = array();
        $this->terms = array();
    }

    public function setAbstract($abstract)
    {
        $this->abstract = html_entity_decode($abstract, ENT_QUOTES, "UTF-8");;
        return $this;
    }

    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
        return $this;
    }

    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    public function setID($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setParentID($parent_id)
    {
        $this->parent_id = $parent_id;
        return $this;
    }

    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    // GETTERS
    public function getID()
    {
        return $this->id;
    }

    public function getParentID()
    {
        return $this->parent_id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function addExtraField($name, $value)
    {
        $this->fields[] = array(
            'name' => $name,
        	'value' => $value,
        );
    }

    public function addTerm(TaxonomyTerm $term)
    {
        $this->terms[] = $term;
        return $this;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
        	'date' => $this->date,
            'title' => $this->title,
        	'language' => $this->language,
            'abstract' => $this->abstract,
        	'content_type' => $this->content_type,
            'url' => $this->url,
            'body' => $this->body,
            'fields' => $this->fields
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement("content");

        $element->setAttribute('id', $this->id);
        $element->setAttribute('parent_id', $this->parent_id);
        $element->setAttribute('date', $this->date);
        $element->setAttribute('language', $this->language);
        $element->setAttribute('content_type', $this->content_type);
        $element->setAttribute('url', $this->url);

        // Title
        $title = $doc->createElement('title', $this->title);
        $element->appendChild($title);

        // Abstract
        $abstract = $doc->createElement('abstract');
        $abstract_cdata = new DOMCdataSection($this->abstract);
        $abstract->appendChild($abstract_cdata);
        $element->appendChild($abstract);

        // Body
        $body = $doc->createElement('body');
        $body_cdata = new DOMCdataSection($this->body);
        $body->appendChild($body_cdata);
        $element->appendChild($body);

        $fields = $doc->createElement('fields');

        foreach ($this->fields as $field) {
            $field_element = $doc->createElement('field');
            $field_element->setAttribute('name', $field['name']);
            $field_element->setAttribute('value', $field['value']);

            $fields->appendChild($field_element);
        }

        $element->appendChild($fields);

        // Terms
        $terms = $doc->createElement('terms');

        foreach ($this->terms as $term) {
            $terms->appendChild($term->export($doc));
        }

        $element->appendChild($terms);

        return $element;
    }
}