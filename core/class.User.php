<?php

class User implements IExportable
{
    protected $id, $username, $password, $email, $map_id, $is_admin;

    public function __construct($id, $username, $password, $email)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->map_id = 0;
        $this->is_admin = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getNewPassword()
    {
        return 'ridicolo';
        return $this->username.'='.$this->email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setMapId($map_id)
    {
        $this->map_id = $map_id;
        return $this;
    }

    public function getMapId()
    {
        return $this->map_id;
    }

    public function setIsAdmin()
    {
        $this->is_admin = true;
        return $this;
    }

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'username' => $this->username,
            'password' => $this->password,
            'email' => $this->email,
            'map_id' => $this->map_id,
            'is_admin' => $this->is_admin
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement( "user" );

        foreach ($this->toArray() as $key => $value) {
            $element->setAttribute($key, $value);
        }

        return $element;
    }
}
