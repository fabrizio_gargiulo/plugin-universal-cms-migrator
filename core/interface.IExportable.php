<?php

interface IExportable
{
    public function export(DOMDocument $doc);
}