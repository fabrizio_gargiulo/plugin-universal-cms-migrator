<?php

class DataBase
{
    static $instance = array();

    protected $db_link, $db_host, $db_name, $db_user, $db_password;

    public function __construct($db_host, $db_name, $db_user, $db_password)
    {
        $this->db_host = $db_host;
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_password = $db_password;

        $this->_connect_db();
    }

    protected function _connect_db()
    {
        logga("Connecting to database: ".$this->db_name);

        $this->db_link = mysql_connect($this->db_host, $this->db_user, $this->db_password);

        if (empty($this->db_link)) {
            throw new Exception("DB not connected: ".$this->db_name);
        }

        $db_connected = mysql_select_db($this->db_name, $this->db_link);

        if (empty($db_connected)) {
            throw new Exception("Could not select database: ".$this->db_user);
        }
    }

    public static function getInstance($db_host, $db_name, $db_user, $db_password)
    {
        if (empty(self::$instance[$db_name])) {
            //$instance = new static();
            self::$instance[$db_name] = new DataBase($db_host, $db_name, $db_user, $db_password);
        }

        return self::$instance[$db_name];
    }

    // Details
    public function getName()
    {
        return $this->db_name;
    }

    public function getCompleteTableName($table_name)
    {
        return sprintf("`%s`.`%s`", $this->getName(), $table_name);
    }

    // Query
    public function query($query)
    {
        // TODO
        logga($query);
        return mysql_query($query, $this->db_link);
    }

    public function getRecords($query)
    {
        $records = array();

        $result = $this->query($query);

        if (!$result) {
            throw new Exception("Wrong query: ".$query);
        }

        while ($record = mysql_fetch_assoc($result)) {
            $records[] = $record;
        }

        return $records;
    }

    // Insert
    public function insert($table_name, $fields)
    {
        $columns = array();
        $values = array();

        foreach (array_keys($fields) as $field) {
            $columns[] = "`".$field."`";
        }

        foreach ($fields as $value) {
            if (in_array($value, array('NULL', 'NOW()'))) {
                $values[] = $value;
                continue;
            }

            if (substr($value, 0, 3) == 'MD5') {
                $values[] = $value;
                continue;
            }

            $values[] = $value == 'NOW()' ? 'NOW()': '"'.addslashes($value).'"';
        }

        $query = sprintf("INSERT INTO %s (%s) VALUES(%s);", $table_name, implode(', ', $columns), implode(', ', $values));
        $this->query($query);

        return mysql_insert_id($this->db_link);
    }

    // Delete
    public function delete($table_name, $where)
    {
        $query = sprintf("DELETE FROM %s WHERE %s", $table_name, $where);
        return $this->query($query);
    }
}