<?php

// SOLO ESPORTAZIONE
abstract class AbstractCMSImporter extends AbstractCMS
{
    protected $maps, $doc, $xpath;

    public function __construct()
    {
        $this->maps = array(
            'users' => array(),
        	'posts' => array(),
        );

        $this->doc = null;
        $this->xpath = null;
    }

    protected function _create_doc($xml_file)
    {
        $this->doc = new DOMDocument();
        $this->doc->recover = TRUE;

        libxml_use_internal_errors(true);

        $this->doc->load($xml_file);
        libxml_clear_errors();

        $this->xpath = new DOMXPath($this->doc);
    }

    protected function _get_elements($query, DOMElement $contextnode = null)
    {
        return $this->xpath->query($query, $contextnode);
    }

    protected function _get_element($query, DOMElement $contextnode = null)
    {
        $list = $this->_get_elements($query, $contextnode);

        if ($list->length > 0) {
            return $list->item(0);
        }

        var_dump($list->length);
        die("cazz: ".$query);

        return null;
    }

    protected function _get_element_value($query, DOMElement $contextnode = null)
    {
        $item = $this->_get_element($query, $contextnode);

        return is_null($item) ? '' : $item->nodeValue;
    }

    abstract public function import($xml_file);

    //abstract public function createPostType();
    //abstract public function createPost();

    // Mapping
    public function mapTaxonomy($source_id, $destination_id)
    {
        $this->maps['taxonomies'][$source_id] = $destination_id;
        return $this;
    }

    public function mapTaxonomyTerm($source_id, $destination_id)
    {
        $this->maps['taxonomie_terms'][$source_id] = $destination_id;
        return $this;
    }

    public function getMappedTaxonomyTerm($source_id)
    {
        return $this->maps['taxonomie_terms'][$source_id];
    }

    public function mapUser($source_id, $destination_id)
    {
        $this->maps['users'][$source_id] = $destination_id;
        return $this;
    }

    public function mapPost($source_id, $destination_id)
    {
        $this->maps['posts'][$source_id] = $destination_id;
        return $this;
    }

    public function getMappings()
    {
        return $this->maps;
    }
}