<?php

class Taxonomy implements IExportable
{
    // Mandatory
    protected $id, $name, $machine_name, $terms;

    // Optional
    protected $description;

    public function __construct($id, $machine_name, $name)
    {
        $name = strtolower($name);
        $name = str_replace(" ", "-", $name);

        $this->id = $id;
        $this->name = $name;
        $this->machine_name = $machine_name;
        $this->description = "";
        $this->terms = array();
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    /*
    public function addTerm($id, $name, $description)
    {
        return new CMSDrupalTaxonomyTerm($this, $id, $name, $description);
    }
    */

    public function addTerm(TaxonomyTerm $term)
    {
        $this->terms[] = $term;
        return $this;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
            'name' => $this->name,
        	'description' => $this->description,
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement("taxonomy");

        $element->setAttribute('id', $this->id);
        $element->setAttribute('name', $this->name);
        $element->setAttribute('machine_name', $this->machine_name);
        $element->setAttribute('description', $this->description);

        // Terms
        $terms = $doc->createElement('terms');

        foreach ($this->terms as $term) {
            $terms->appendChild($term->export($doc));
        }

        $element->appendChild($terms);

        return $element;
    }
}