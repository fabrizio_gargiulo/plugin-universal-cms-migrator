<?php

class TaxonomyTerm implements IExportable
{
    // Mandatory
    protected $taxonomy, $id, $name, $machine_name;

    // Optional
    protected $parent, $description;

    public function __construct(Taxonomy $taxonomy, $id, $name, $machine_name)
    {
        $this->taxonomy = $taxonomy;
        $this->id = $id;
        $this->parent = 0;
        $this->name = $name;
        $this->machine_name = $machine_name;
        $this->description = "";
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function toArray()
    {
        return array(
            'id' => $this->id,
        	'parent' => $this->parent,
            'name' => $this->name,
        	'machine_name' => $this->machine_name,
        	'description' => $this->description,
        );
    }

    public function export(DOMDocument $doc)
    {
        $element = $doc->createElement("term");

        $element->setAttribute('taxonomy_id', $this->taxonomy->getID());
        $element->setAttribute('taxonomy_name', $this->taxonomy->getName());
        $element->setAttribute('id', $this->id);
        $element->setAttribute('parent', $this->parent);
        $element->setAttribute('name', $this->name);
        $element->setAttribute('machine_name', $this->machine_name);
        $element->setAttribute('description', $this->description);

        return $element;
    }
}